# README #

This is the repository where I keep all of my Mainframe related files

## [Watch the Wiki(Summary of the Topics)](https://bitbucket.org/jelledebock/mainframe-files/wiki/)##

## Content ##
1. [Employee VSAM read (exercise 7b of exercises)](https://bitbucket.org/jelledebock/mainframe-files/src/9cc19917871a7947b83a3682d17cc337f323d334/PEMP/?at=master)
2. [Employee VSAM READ LIST and SEPERATE RECORD (exercise 7c of exercises)](https://bitbucket.org/jelledebock/mainframe-files/src/255f907dea4ef63f6730b1d19209acd1ce92d0f5/IEMP/?at=master)
3. [BABEL2 with external subprogram TRANSLT](https://bitbucket.org/jelledebock/mainframe-files/src/8af71b893a79d8afb37ac325d218b66f75039b9c/BABEL2/?at=master)
4. [CHEF with internal subprogram](https://bitbucket.org/jelledebock/mainframe-files/src/605adfccee5f0599ba7f5f12d12115c08dab4b97/CHEF/?at=master)
5. [All the JCL exercises I made](https://bitbucket.org/jelledebock/mainframe-files/src/318bb1c5bb075f612308b559a79c50a3ef866a55/JCL-exercises/?at=master)
6. [DB2-COBOL Compilation Schema](https://bytebucket.org/jelledebock/mainframe-files/raw/4943e4687e9d401aca5de8bc16b96bed935ab073/Databanken-trans/DB2-compilatie-schema.pdf)
7. [Summary of the CICS chapter of DB/TRANS](https://bytebucket.org/jelledebock/mainframe-files/raw/7fdcf768a51526632311dff5d37b1a6c938644e0/Databanken-trans/CICS_summary.pdf)
### For who is this repository? ###

*Students Hogent Mainframe
*People who want to waste their times learning old stuff ;)

### What will be in this REPO? ###
* JCL
* COBOL
* REXX
* DB2SQL
* Screenshots of processes