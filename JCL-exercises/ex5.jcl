//HOG0009A JOB ,'J DE BOCK',NOTIFY=&SYSUID                              00000100
//* DATAGENERATION PART 1                                               00000701
//CREATE EXEC PGM=IEBDG                                                 00000800
//SYSPRINT DD SYSOUT=*                                                  00000900
//UITVOER DD DSN=HOG0009.OUTPUT.TESTOUT,DISP=(NEW,CATLG),               00001000
// UNIT=SYSDA,SPACE=(CYL,(1,1)),DCB=(DSORG=PS,RECFM=FB,LRECL=150)       00001100
//SYSIN DD *                                                            00001200
 DSD OUTPUT=(UITVOER)                                                   00001300
 FD  NAME=NAAM,LENGTH=30,FORMAT=AN,ACTION=RP                            00001400
 FD  NAME=FILLER,LENGTH=1,FILL=' '                                      00001500
 FD  NAME=STREETA,LENGTH=30,FORMAT=AN,ACTION=RP                         00001600
 FD  NAME=HOUSEN,LENGTH=4,FORMAT=ZD,INDEX=1,CYCLE=2                     00001700
 CREATE QUANTITY=30,FILL=X'00',NAME=(NAAM,FILLER,STREETA,FILLER,HOUSEN) 00001800
 END                                                                    00001900
/*                                                                      00002000
//* DATAGENERATION PART 2                                               00002101
//CREATE2 EXEC PGM=IEBDG,COND=(0,NE,CREATE)                             00002201
//SYSPRINT DD SYSOUT=*                                                  00002301
//UITVOER2 DD DSN=HOG0009.OUTPUT.TESTOUT2,DISP=(NEW,CATLG),             00002402
// UNIT=SYSDA,SPACE=(CYL,(1,1)),DCB=(DSORG=PS,RECFM=FB,LRECL=150)       00002501
//SYSIN DD *                                                            00002601
 DSD OUTPUT=(UITVOER2)                                                  00002701
 FD  NAME=NAAM,LENGTH=30,FORMAT=AN,ACTION=RP                            00002801
 FD  NAME=FILLER,LENGTH=1,FILL=' '                                      00002901
 FD  NAME=STREETA,LENGTH=30,FORMAT=AN,ACTION=RP                         00003001
 FD  NAME=HOUSEN,LENGTH=4,FORMAT=ZD,INDEX=1,CYCLE=2                     00003101
 CREATE QUANTITY=30,FILL=X'00',NAME=(NAAM,FILLER,STREETA,FILLER,HOUSEN) 00003201
 END                                                                    00003301
/*                                                                      00003401
//* THE SORTING STEP                                                    00003501
//SORTCND IF RC = 0 THEN                                                00003602
//SORT EXEC PGM=SORT                                                    00003701
//SORTIN DD DISP=SHR,DSN=HOG0009.OUTPUT.TESTOUT                         00003801
//       DD DISP=SHR,DSN=HOG0009.OUTPUT.TESTOUT2                        00003901
//SORTOUT DD DSN=HOG0009.OUTPUT.SORTED,DISP=(NEW,CATLG),                00004001
//   UNIT=SYSDA,SPACE=(CYL,(1,1)),DCB=(DSORG=PS,RECFM=FB,LRECL=80)      00004101
//SYSOUT DD SYSOUT=*                                                    00004201
//SYSIN  DD *                                                           00004301
  SORT FIELDS=(1,30,CH,A)                                               00004401
/*                                                                      00005001
//* DELETE THE ORIGINAL DATA SET                                        00006001
//DELETE EXEC PGM=IEFBR14                                               00007001
//A      DD DISP=(OLD,DELETE),DSN=HOG0009.OUTPUT.TESTOUT                00008001
//B      DD DISP=(OLD,DELETE),DSN=HOG0009.OUTPUT.TESTOUT2               00009001
// ENDIF                                                                00010002
