//HOG0009J JOB ,'J DE BOCK',NOTIFY=&SYSUID                              00010001
//JOBLIB DD DSN=HOG0009.EXAMPLES.LOAD,DISP=SHR                          00020001
//STEP1 EXEC PGM=RCRET                                                  00030000
//SYSOUT DD SYSOUT=*                                                    00040007
//DATAIN DD *                                                           00050016
0                                                                       00060019
/*                                                                      00070019
// IF STEP1.ABEND THEN                                                  00071009
//STEPAB EXEC PGM=IEBGENER                                              00072005
//SYSIN DD DUMMY                                                        00073005
//SYSUT1 DD *                                                           00074005
PROGRAM ABENDED                                                         00075005
/*                                                                      00076005
//SYSUT2 DD SYSOUT=*                                                    00077005
//SYSPRINT DD SYSOUT=*                                                  00078005
// ENDIF                                                                00079005
// IF STEP1.RC >= 8 THEN                                                00080001
//STEP2A EXEC PGM=IEBGENER                                              00090004
//SYSIN DD DUMMY                                                        00100002
//SYSUT1 DD *                                                           00110002
RETURNCODE GREATER THEN 8                                               00111002
/*                                                                      00112003
//SYSUT2 DD SYSOUT=*                                                    00113002
//SYSPRINT DD SYSOUT=*                                                  00114002
// ELSE                                                                 00120001
//STEP2B EXEC PGM=IEBGENER                                              00121004
//SYSIN DD DUMMY                                                        00122002
//SYSUT1 DD *                                                           00123002
RETURNCODE LESS THEN 8                                                  00124002
/*                                                                      00125003
//SYSUT2 DD SYSOUT=*                                                    00126002
//SYSPRINT DD SYSOUT=*                                                  00127002
// ENDIF                                                                00160002
