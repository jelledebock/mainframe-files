//HOG0009J JOB ,'J DE BOCK',NOTIFY=&SYSUID                              00010001
//JOBLIB DD DSN=HOG0009.EXAMPLES.LOAD,DISP=SHR                          00020001
//STEP1 EXEC PGM=RCRET                                                  00030001
//SYSOUT DD SYSOUT=*                                                    00040001
//DATAIN DD *                                                           00050001
99                                                                      00060003
/*                                                                      00070001
//STEPAB EXEC PGM=IEBGENER,COND=ONLY                                    00072002
//SYSIN DD DUMMY                                                        00073001
//SYSUT1 DD *                                                           00074001
PROGRAM ABENDED                                                         00075001
/*                                                                      00076001
//SYSUT2 DD SYSOUT=*                                                    00076105
//SYSPRINT DD SYSOUT=*                                                  00077004
//STEP2A EXEC PGM=IEBGENER,COND=(8,GT)                                  00090002
//SYSIN DD DUMMY                                                        00100001
//SYSUT1 DD *                                                           00110001
RETURNCODE GREATER THEN 8                                               00111001
/*                                                                      00112001
//SYSUT2 DD SYSOUT=*                                                    00113001
//SYSPRINT DD SYSOUT=*                                                  00114001
//STEP2B EXEC PGM=IEBGENER,COND=(8,LE)                                  00116002
//SYSIN DD DUMMY                                                        00117001
//SYSUT1 DD *                                                           00118001
RETURNCODE LESS THEN 8                                                  00119001
/*                                                                      00120001
//SYSUT2 DD SYSOUT=*                                                    00121001
//SYSPRINT DD SYSOUT=*                                                  00122001
