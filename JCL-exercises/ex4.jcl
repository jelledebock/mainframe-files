//HOG0009A JOB ,'J DE BOCK',NOTIFY=&SYSUID                              00000100
//CREATE EXEC PGM=IEBDG                                                 00000200
//SYSPRINT DD SYSOUT=*                                                  00000300
//UITVOER DD DSN=HOG0009.CUSTM,DISP=(NEW,CATLG),                        00000401
// UNIT=SYSDA,SPACE=(TRK,(1,1)),DCB=(DSORG=PS,RECFM=FB,LRECL=150)       00000501
//SYSIN DD *                                                            00000600
 DSD OUTPUT=(UITVOER)                                                   00000700
 FD  NAME=NAAM,LENGTH=30,FORMAT=AN,ACTION=RP                            00000800
 FD  NAME=FILLER,LENGTH=1,FILL=' '                                      00000900
 FD  NAME=STREETA,LENGTH=30,FORMAT=AN,ACTION=RP                         00001000
 FD  NAME=HOUSEN,LENGTH=4,FORMAT=ZD,INDEX=1,CYCLE=2                     00001200
 CREATE QUANTITY=30,FILL=X'00',NAME=(NAAM,FILLER,STREETA,FILLER,HOUSEN) 00002000
 END                                                                    00004000
//CREATE EXEC PGM=IEBDG                                                 00005001
//SYSPRINT DD SYSOUT=*                                                  00005101
//UITVOER2 DD DSN=HOG0009.CUSTM2,DISP=(NEW,CATLG),                      00005201
// UNIT=SYSDA,SPACE=(TRK,(1,1)),DCB=(DSORG=PS,RECFM=FB,LRECL=150)       00005301
//SYSIN DD *                                                            00005401
 DSD OUTPUT=(UITVOER2)                                                  00005501
 FD  NAME=NAAM,LENGTH=30,FORMAT=AN,ACTION=RP                            00005601
 FD  NAME=FILLER,LENGTH=1,FILL=' '                                      00005701
 FD  NAME=STREETA,LENGTH=30,FORMAT=AN,ACTION=RP                         00005801
 FD  NAME=HOUSEN,LENGTH=4,FORMAT=ZD,INDEX=1,CYCLE=2                     00005901
 CREATE QUANTITY=30,FILL=X'00',NAME=(NAAM,FILLER,STREETA,FILLER,HOUSEN) 00006001
 END                                                                    00006101
//VERW EXEC PGM=SORT                                                    00006201
//SYSPRINT DD SYSOUT=*                                                  00006301
//VERW EXEC PGM=IEFBR14                                                 00006401
//SYSPRINT DD SYSOUT=*                                                  00006501
//DELDD DD DSN=HOG0009.CUSTM,DISP=(OLD,DELETE,DELETE)                   00007001
//DELDD2 DD DSN=HOG0009.CUSTM,DISP=(OLD,DELETE,DELETE)                  00008001
