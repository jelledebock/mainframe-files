/* REXX */
/* Go through stack, but leave the stack unmodified */
/* You can do this in 2 simple steps 
REQUIRED: passing the number of stack elements
*/

/* STEP 1 ----------- COPY YOUR PULLS TO A STEM VARIABELE */
mymax: procedure
parse arg count

do i = 1 to count
	parse pull lines.i
	/* Her you can do something usefull with your pulled values */
end

/* STEP 2 ----------- Put your values back on the stack (in the correct order) */

do i = 1 to count
 	push lines.i
end

return something
