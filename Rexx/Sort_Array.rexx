/* REXX */
/* Algorithm to sort an array */
/* Usage: */
/* create foo.*/                                     
/* call sort1 */
/* foo. is now sorted */

sort1: procedure expose foo.
 n = foo.0
 do i = 1 to n
    do j = i+1 to n
       if foo.i > foo.j then do
          temp = foo.i
          foo.i = foo.j
          foo.j = temp
       end
    end
 end
 return